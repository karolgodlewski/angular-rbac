import * as mongoose from 'mongoose';

const featureModel = {
  name: String,
  status: Number,
  statusText: String,
  requiredClaims: [String],
  children: [
    {
      name: String,
      requiredClaims: [String]
    }
  ]
};

export const FeatureSchema =  mongoose.Schema(featureModel);

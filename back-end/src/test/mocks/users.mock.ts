import {UserDto} from 'src/users/dto/users.dto';
import {userRolesEnum} from '../../users/enums/user-roles.enum';

export const usersMock: UserDto[] = [
  {
    _id: 'ysai7t8sat86ra',
    username: userRolesEnum.SUPER_ADMIN,
    password: 'pass',
    role: userRolesEnum.SUPER_ADMIN,
    claims: [
      'VIEW_HOME',
      'VIEW_ADMIN_PANEL',
      'USE_HOME__SUPER_ADMIN_BTN'
    ]
  },
  {
    _id: '097knfut6fegkjs',
    username: userRolesEnum.ADMIN,
    password: 'pass',
    role: userRolesEnum.ADMIN,
    claims: [
      'VIEW_HOME',
      'VIEW_ADMIN_PANEL',
      'USE_HOME__ADMIN_BTN'
    ]
  },
  {
    _id: '99jlkniut78tdsgh',
    username: userRolesEnum.REGULAR,
    password: 'pass',
    role: userRolesEnum.REGULAR,
    claims: [
      'VIEW_HOME'
    ]
  }
];

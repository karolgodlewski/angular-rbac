export const hasRequiredClaims = (requiredClaims: string[], claims: string[]): { status: boolean; commonClaims: string[] } => {
  const commonClaims = requiredClaims.filter(claim => claims.includes(claim));
  return {
    status: !requiredClaims.length || !!commonClaims.length,
    commonClaims
  };
};

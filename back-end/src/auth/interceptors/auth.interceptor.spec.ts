import {AuthInterceptor} from './auth.interceptor';
import {of} from 'rxjs';

const resHeaders = {};
const fakeContext = {
  switchToHttp: jest.fn().mockReturnThis(),
  getResponse: jest.fn().mockReturnValue({
    header(headerName: string, value: string) {
      resHeaders[headerName] = value;
    }
  }),
  getRequest: jest.fn().mockReturnThis()
};

const fakeNext = {
  handle: jest.fn()
};

xdescribe('AuthInterceptor', () => {
  let interceptor: AuthInterceptor;

  beforeEach(() => {
    interceptor = new AuthInterceptor();
  });

  describe('intercept', () => {
    it('should set authorization header', async () => {
      fakeContext.switchToHttp().getRequest.mockReturnValueOnce({
        headers: {
          Authorization: 'test auth token'
        }
      });
      // fakeContext.switchToHttp().getResponse.mockReturnValueOnce({
      // 	headers: {}
      // });
      jest.spyOn(fakeNext, 'handle').mockReturnValue(of(true));
      await interceptor.intercept(fakeContext as any, fakeNext as any);
      expect(resHeaders['Authorization']).toBe('test auth token');
    });
  });
});

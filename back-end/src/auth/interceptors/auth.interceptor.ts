import {CallHandler, ExecutionContext, Injectable, NestInterceptor} from '@nestjs/common';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    return next.handle()
      .pipe(
        map(data => {
          const req = context.switchToHttp().getRequest();
          const res = context.switchToHttp().getResponse();
          const authHeader = req.header('Authorization');
          // tslint:disable-next-line: no-unused-expression
          authHeader && res.header('Authorization', authHeader);
          return data;
        })
      );
  }
}

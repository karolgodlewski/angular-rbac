import {Module, Global} from '@nestjs/common';
import {AuthTokenService} from '././services/auth-token/auth-token.service';
import {AuthController} from './auth.controller';
import {MongooseModule} from '@nestjs/mongoose';
import {FeatureSchema} from '../schemas/feature.schema';
import {UsersModule} from '../users/users.module';

@Global()
@Module({
imports: [
    MongooseModule.forFeature([
      {name: 'Feature', schema: FeatureSchema}
    ]),
    UsersModule
  ],
  providers: [AuthTokenService],
  controllers: [AuthController],
  exports: [AuthTokenService]
})
export class AuthModule {}

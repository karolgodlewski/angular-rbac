import {Controller, Post, Body, Res, Req, Get, UseGuards, UseInterceptors, Query, HttpException} from '@nestjs/common';
import {CredentialsDto} from './dto/credentials.dto';
import {UsersService} from '../users/users.service';
import {UserDto} from 'src/users/dto/users.dto';
import {AuthTokenService} from './services/auth-token/auth-token.service';
import {AuthGuard} from './guards/auth.guard';
import {AuthInterceptor} from './interceptors/auth.interceptor';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {ParentFeatureDto} from './dto/feature.dto';
import {hasRequiredClaims} from './helpers/has-required-claims';
import {ParentFeature} from './models/feature.interface';

@Controller('')
export class AuthController {
  constructor(
    @InjectModel('Feature') private readonly featureModel: Model<ParentFeature>,
    private userService: UsersService,
    private tokenService: AuthTokenService
  ) { }

  @Get('login')
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthInterceptor)
  async prolongSession(@Req() req): Promise<{ token: string; user: UserDto }> {
    const authHeader = req.header('Authorization');
    const token = authHeader.split(' ')[1];
    const {issuedFor} = this.tokenService.get({token})[0];
    const user = await this.userService.getUserById(issuedFor);
    return {user, token};
  }

  @Post('login')
  async login(@Body() credentials: CredentialsDto, @Res() res): Promise<{ token: string; user: UserDto }> {
    const user = await this.userService.login(credentials);
    const {token} = this.tokenService.create(user._id);
    res.header('Authorization', `Bearer ${token}`);
    return res.send({token, user});
  }

  @Post('logout')
  logout(@Req() req): { session: boolean } {
    const authHeader = req.header('Authorization');
    let session = true;
    if (authHeader) {
      const token = authHeader.split(' ')[1];
      session = !this.tokenService.delete(token);
    } else {
      session = false;
    }
    return {session};
  }

  @Get('feature-availability')
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthInterceptor)
  async getFeatureAvailability(@Query('name') name: string, @Req() req): Promise<{ [key: string]: ParentFeatureDto; APP: ParentFeatureDto }> {
    const feature: ParentFeatureDto  = await this.featureModel.findOne({name});
    const appFeature: ParentFeatureDto = await this.featureModel.findOne({name: 'APP'});
    const featureInMaintanence = !feature.status && feature.statusText === 'MAINTENANCE';
    const appFeatureInMaintanence = !appFeature.status && appFeature.statusText === 'MAINTENANCE';
    if (appFeatureInMaintanence || featureInMaintanence) {
      throw new HttpException({
        status: 403,
        error: 'maintenance',
        redirectTo: '/maintenance'
      }, 403);
    }
    const {userId} = req.body;
    if (name !== 'APP') {
      const user = await this.userService.getUserById(userId);
      const claims = hasRequiredClaims(feature.requiredClaims, user.claims);
      if (!claims.status) {
        throw new HttpException({
          status: 403,
          error: 'access denied',
          redirectTo: '/access-denied'
        }, 403);
      }
    }
    return {[feature.name]: feature, APP: appFeature};
  }
}

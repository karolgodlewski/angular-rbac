import {CanActivate, ExecutionContext, Injectable, HttpException} from '@nestjs/common';
import {Observable} from 'rxjs';
import {AuthTokenService} from '../services/auth-token/auth-token.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private tokenService: AuthTokenService) { }

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    const authHeader = req.header('Authorization');
    const authHeaderValid = authHeader && authHeader.startsWith('Bearer');
    if (!authHeaderValid) {
      throw new HttpException({
        status: 401,
        error: 'Unauthorized - invalid auth header'
      }, 401);
    }
    const token = authHeader.split(' ')[1];
    if (!this.tokenService.validate(token)) {
      throw new HttpException({
        status: 401,
        error: 'Unauthorized - invalid auth token'
      }, 401);
    }
    const {issuedFor} = this.tokenService.get({token})[0];
    req.body.userId = issuedFor;
    return true;
  }
}

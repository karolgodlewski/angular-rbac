import {Injectable} from '@nestjs/common';
import {AuthTokenDto} from '../../dto/auth-token.dto';

@Injectable()
export class AuthTokenService {
  private _tokens: AuthTokenDto[] = [];

  create(userId: string): AuthTokenDto {
    const token = userId + new Date().getTime().toString();
    const tokenEntry = {token, issuedFor: userId};
    this._tokens = [...this._tokens, tokenEntry];
    return tokenEntry;
  }

  get({userId, token}: { userId?: string; token?: string }): AuthTokenDto[] {
    let predicate = t => false;
    if (userId && token) {
      predicate = t =>
        t.issuedFor === userId &&
        t.token === token
      ;
    } else if (userId) {
      predicate = t => t.issuedFor === userId;
    } else if (token) {
      predicate = t => t.token === token;
    }
    const entries = this._tokens.filter(predicate);
    return entries;
  }

  delete(token: string): boolean {
    this._tokens = this._tokens.filter(t => t.token !== token);
    return !this._tokens.find(t => t.token === token);
  }

  validate(token: string): boolean {
    const tokenEntry = this._tokens.find(t => t.token === token);
    return !!tokenEntry;
  }
}

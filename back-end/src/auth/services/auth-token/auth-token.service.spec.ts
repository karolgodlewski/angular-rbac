import {Test, TestingModule} from '@nestjs/testing';
import {AuthTokenService} from './auth-token.service';
import {usersMock} from '../../../test/mocks/users.mock';

describe('AuthTokenService', () => {
  let tokenService: AuthTokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthTokenService]
    }).compile();
    tokenService = module.get<AuthTokenService>(AuthTokenService);
  });

  describe('create', () => {
    it('should create auth token for specified user id', () => {
      const userId = usersMock[1]._id;
      const ret = tokenService.create(userId);
      expect(ret).toEqual(tokenService.get({userId})[0]);
      expect(ret.issuedFor).toBe(userId);
    });
  });

  describe('get', () => {
    it('should retrieve tokens for specified user id', () => {
      const userId = usersMock[1]._id;
      const {token} = tokenService.create(userId);
      const ret = tokenService.get({userId});
      const tokenEntry = ret.find(t => t.token === token);
      expect(tokenEntry).toBeDefined();
    });
  });

  describe('delete', () => {
    it('should return true for valid token', () => {
      const userId = usersMock[1]._id;
      const {token} = tokenService.create(userId);
      const ret = tokenService.delete(token);
      const tokens = tokenService.get({userId});
      const tokenEntry = tokens
        .find(t => t.issuedFor === token);
      expect(ret).toBe(true);
      expect(tokenEntry).toBeFalsy();
    });
  });

  describe('validate', () => {
    it('should return true for valid token', () => {
      const userId = usersMock[1]._id;
      const {token} = tokenService.create(userId);
      const ret = tokenService.validate(token);
      expect(ret).toBe(true);
    });

    it('should return false for invalid token', () => {
      const ret = tokenService.validate('invalid_token');
      expect(ret).toBe(false);
    });
  });
});

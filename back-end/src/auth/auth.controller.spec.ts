import {Test, TestingModule} from '@nestjs/testing';
import {AuthController} from './auth.controller';
import {UsersService} from '../users/users.service';
import {usersMock} from '../test/mocks/users.mock';
import {CredentialsDto} from './dto/credentials.dto';
import {AuthTokenService} from '././services/auth-token/auth-token.service';
import {AuthTokenDto} from './dto/auth-token.dto';
import {featureNamesEnum} from '../enums/feature-names.enum';
import {featuresMock} from '../test/mocks/features.mock';
import * as mongoose from 'mongoose';
import {getModelToken, MongooseModule} from '@nestjs/mongoose';
import {FeatureSchema} from '../schemas/feature.schema';
import {ParentFeatureDto} from './dto/feature.dto';
import {ParentFeature} from './models/feature.interface';

const fakeRes = {
  header: jest.fn(),
  send: jest.fn()
};

const fakeReq = {
  header: jest.fn(),
  send: jest.fn()
};

const FakeUserService = {
  getUsers: jest.fn(),
  getUserById: jest.fn(),
  login: jest.fn()
};

describe('Auth Controller', () => {
  let controller: AuthController;
  let usersService: UsersService;
  let tokenService: AuthTokenService;
  let featureModel: mongoose.Model<ParentFeatureDto>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {provide: UsersService, useValue: FakeUserService},
        {
          provide: getModelToken('Feature'),
          useValue: mongoose.model('Feature', FeatureSchema)
        },
        AuthTokenService
      ]
    }).compile();

    controller = module.get<AuthController>(AuthController);
    usersService = module.get<UsersService>(UsersService);
    tokenService = module.get<AuthTokenService>(AuthTokenService);
    featureModel = module.get<mongoose.Model<ParentFeature>>('Feature');
  });

  describe('POST login', () => {
    it('should return user of provided credentials', async () => {
      const user = usersMock[1];
      const sendSpy = jest.spyOn(fakeRes, 'send');
      const credentials: CredentialsDto = {
        username: user.username,
        password: user.password
      };
      const tokenServiceCreateReturnValue = {token: 'valid_token', issuedFor: user._id};
      jest.spyOn(tokenService, 'create').mockReturnValue(tokenServiceCreateReturnValue);
      jest.spyOn(FakeUserService, 'login').mockImplementation(() => Promise.resolve(usersMock[1]));
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      await controller.login(credentials, fakeRes as any);
      expect(sendSpy).toHaveBeenCalledWith({user: usersMock[1], token: tokenServiceCreateReturnValue.token});
    });
  });

  describe('GET login', () => {
    it('should prolong user session', async () => {
      const user = usersMock[1];
      const token = 'valid_token';
      const mockTokenServiceRetValue = [{issuedFor: user._id, token} as AuthTokenDto];
      jest.spyOn(fakeReq, 'header').mockReturnValue('Bearer valid_token');
      jest.spyOn(tokenService, 'get').mockReturnValue(mockTokenServiceRetValue);
      jest.spyOn(FakeUserService, 'getUserById').mockReturnValue(Promise.resolve(user));
      const ret = await controller.prolongSession(fakeReq);
      expect(ret).toEqual({user, token});
    });
  });

  describe('POST logout', () => {
    it('should remove auth token', () => {
      jest.spyOn(tokenService, 'delete').mockReturnValue(true);
      jest.spyOn(fakeReq, 'header').mockReturnValue('Bearer valid_token');
      const ret = controller.logout(fakeReq);
      expect(ret).toEqual({session: false});
    });
  });

  describe('GET feature-availability', () => {
    it('should return availability definitions for provided name', async () => {
      const ret = await controller.getFeatureAvailability(featureNamesEnum.HOME, fakeRes);
      expect(ret).toEqual({data: featuresMock[featureNamesEnum.HOME]});
    });
  });
});

import {ParentFeature, Feature} from '../models/feature.interface';

export class ParentFeatureDto implements ParentFeature {
  readonly _id: string;
  name: string;
  status: number;
  statusText: string;
  requiredClaims: string[];
  children: Feature[];
}

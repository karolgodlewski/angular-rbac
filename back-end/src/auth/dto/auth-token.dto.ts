export class AuthTokenDto {
  token: string;
  issuedFor: string; // user id
}

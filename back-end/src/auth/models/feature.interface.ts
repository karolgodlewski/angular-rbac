export interface Feature {
  readonly _id: string;
  name: string;
  status: number;
  statusText: string;
  requiredClaims: string[];
}

export interface ParentFeature extends Feature {
  children: Feature[];
}

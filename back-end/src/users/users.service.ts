import {Injectable} from '@nestjs/common';
import {UserDto} from './dto/users.dto';
import {CredentialsDto} from 'src/auth/dto/credentials.dto';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<UserDto>) { }

  async getUsers(): Promise<UserDto[]> {
    return await this.userModel.find({});
  }

  async getUserById(id: string): Promise<UserDto> {
    return await this.userModel.findOne({_id: id});
  }

  async login({username, password}: CredentialsDto): Promise<UserDto> {
    return await this.userModel.findOne({username, password});
  }
}

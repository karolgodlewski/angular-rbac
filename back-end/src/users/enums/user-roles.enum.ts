export enum userRolesEnum {
  SUPER_ADMIN = 'super_admin',
  ADMIN = 'admin',
  REGULAR = 'regular'
}

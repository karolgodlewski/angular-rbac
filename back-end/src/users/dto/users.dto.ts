import {User, userRoles} from '../interfaces/user.interface';

export class UserDto implements User {
  readonly _id: string;
  username: string;
  password: string;
  role: userRoles;
  claims: string[];
}

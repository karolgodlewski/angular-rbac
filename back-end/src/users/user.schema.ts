import * as mongoose from 'mongoose';

export const userModel = {
  username: String,
  password: String,
  role: String,
  claims: [String]
};

export const UserSchema = mongoose.Schema(userModel);

import {Controller, Get, Param, UseGuards, UseInterceptors} from '@nestjs/common';
import {UsersService} from './users.service';
import {UserDto} from './dto/users.dto';
import {AuthGuard} from '../auth/guards/auth.guard';
import {AuthInterceptor} from '../auth/interceptors/auth.interceptor';

@Controller('users')
export class UsersController {

  constructor(private usersService: UsersService) { }

  @Get()
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthInterceptor)
  getUsers(): Promise<UserDto[]> {
    return this.usersService.getUsers();
  }

  @Get(':id')
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthInterceptor)
  getUserById(@Param('id') id: string): Promise<UserDto> {
    return this.usersService.getUserById(id);
  }
}

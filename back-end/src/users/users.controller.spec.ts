import {Test, TestingModule} from '@nestjs/testing';
import {UsersController} from './users.controller';
import {UsersService} from './users.service';
import {usersMock} from '../test/mocks/users.mock';
import {AuthTokenService} from '../auth/services/auth-token/auth-token.service';

const FakeUserService = {
  getUsers: jest.fn(),
  getUserById: jest.fn()
};

describe('Users Controller', () => {
  let controller: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {provide: UsersService, useValue: FakeUserService},
        AuthTokenService
      ]
    }).compile();
    controller = module.get<UsersController>(UsersController);
    usersService = module.get<UsersService>(UsersService);
  });

  describe('getUsers', () => {
    it('should retrieve list of users', async () => {
      jest.spyOn(FakeUserService, 'getUsers').mockReturnValue(usersMock);
      const res = await controller.getUsers();
      expect(res).toEqual(usersMock);
    });
  });

  describe('getUserById', () => {
    test('should retrieve user of specified id', async () => {
      jest.spyOn(FakeUserService, 'getUserById').mockImplementation(() => Promise.resolve(usersMock[1]));
      const res = await controller.getUserById(usersMock[1]._id);
      expect(res).toEqual(usersMock[1]);
    });
  });
});

import {userRolesEnum} from '../enums/user-roles.enum';

export type userRoles = userRolesEnum.SUPER_ADMIN | userRolesEnum.ADMIN | userRolesEnum.REGULAR;

export interface User {
  readonly _id: string;
  username: string;
  password: string;
  role: userRoles;
  claims: string[];
}

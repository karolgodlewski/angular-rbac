import {Test, TestingModule} from '@nestjs/testing';
import {UsersService} from './users.service';
import {CredentialsDto} from 'src/auth/dto/credentials.dto';
import {usersMock} from '../test/mocks/users.mock';
import {getModelToken} from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {UserSchema} from './user.schema';
import {UserDto} from './dto/users.dto';

describe('UsersService', () => {
  let service: UsersService;
  let userModel: mongoose.Model<UserDto>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getModelToken('User'),
          useValue: mongoose.model('User', UserSchema)
        }
      ]
    }).compile();

    service = module.get<UsersService>(UsersService);
    // userModel = module.get<mongoose.Model<UserDto>>('User');
  });

  describe('getAllUsers', () => {
    it('should return users list', async () => {
      jest.spyOn(userModel, 'find').mockReturnValue(usersMock);
      const res = await service.getUsers();
      expect(res).toEqual(usersMock);
    });
  });

  describe('getUserById', () => {
    it('should retrieve user of specified id', async () => {
      const res = await service.getUserById(usersMock[1]._id);
      expect(res).toEqual(usersMock[1]);
    });
  });

  describe('login', () => {
    it('should retrieve user of matching credentials', async () => {
      const {username, password} = usersMock[1];
      const credentials: CredentialsDto = {username, password};
      const ret = await service.login(credentials);
      expect(ret).toEqual(usersMock[1]);
    });

    it('should throw error when no user found', async () => {
      const credentials: CredentialsDto = {
        username: 'invalid_user',
        password: 'invalid_password'
      };
      expect(service.login(credentials)).rejects.toEqual(new Error('Invalid credentials'));
    });
  });
});

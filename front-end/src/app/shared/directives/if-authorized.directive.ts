
import { Directive, Input, OnChanges, TemplateRef, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { Subscription } from 'rxjs';
import User from 'src/app/interfaces/user/user.interface';
import { ParentFeature } from 'src/app/interfaces/feature/feature.interface';
import { hasRequiredClaims } from 'src/app/helpers/has-required-claims';

@Directive({
  selector: '[ifAuthorized]'
})
export class IfAuthorizedDirective implements OnInit, OnChanges, OnDestroy {
  @Input() ifAuthorized: ParentFeature;
  private _user: User;
  private _userSub: Subscription;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private userService: UserService
  ) { }

  ngOnInit() {
    this._userSub = this.userService.user$
      .subscribe(({ data }) => {
        this._user = data;
        this._setVisibility();
      });
  }

  ngOnChanges() {
    this._setVisibility();
  }

  ngOnDestroy() {
    this._userSub.unsubscribe();
  }

  private _setVisibility() {
    if (!this._user || !this.ifAuthorized) {
      return this.viewContainer.clear();
    }
    const claims = hasRequiredClaims(this.ifAuthorized.requiredClaims, this._user.claims);
    this.viewContainer.clear();
    if (claims.status) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}

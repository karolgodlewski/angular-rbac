import { async, TestBed } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';
import { UserService } from 'src/app/core/services/user/user.service';
import { IfAuthorizedDirective } from './if-authorized.directive';
import { TemplateRef, ViewContainerRef } from '@angular/core';
import { featuresMock } from 'src/app/mocks/features.mock';
import { usersMock } from 'src/app/mocks/users.mock';
import { featureNamesEnum } from 'src/app/enums/feature-names.enum';

const FakeElementRef =  {
  nativeElement: { }
};
const FakeTemplateRef = {
  elementRef: FakeElementRef
}
const FakeViewContainerRef = {
  elementRef: FakeElementRef,
  clear() { },
  createEmbeddedView() { }
};
const _user = new BehaviorSubject({ data: usersMock[1] });
const FakeUserService = {
  user$: _user.asObservable(),
};

describe('IfAuthorizedDirective', () => {
  let directive;
  let userService: UserService;
  let viewContainerRef: ViewContainerRef;
  let templateRef: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        IfAuthorizedDirective,
        { provide: UserService, useValue: FakeUserService },
        { provide: TemplateRef, useValue: FakeTemplateRef },
        { provide: ViewContainerRef, useValue: FakeViewContainerRef }
      ]
    });
    directive = TestBed.get(IfAuthorizedDirective);
    userService = TestBed.get(UserService);
    viewContainerRef = TestBed.get(ViewContainerRef);
    templateRef = TestBed.get(TemplateRef);
  }));

  beforeEach(() => {
    const feature = featuresMock
      .find((f) => f.name === featureNamesEnum.HOME);
    directive.ifAuthorized = feature.children
      .find((f) => f.name === featureNamesEnum.HOME__ADMIN_BTN);
  });

  describe('ngOnInit', () => {
    it('should display element when user has authorization', () => {
      const clearSpy = spyOn(viewContainerRef, 'clear');
      const createEmbeddedViewSpy = spyOn(viewContainerRef, 'createEmbeddedView');
      directive.ngOnInit();
      expect(createEmbeddedViewSpy).toHaveBeenCalledWith(templateRef);
      expect(clearSpy).toHaveBeenCalledTimes(1);
    });

    it('should remove element when user has no authorization', () => {
      const clearSpy = spyOn(viewContainerRef, 'clear');
      _user.next({ data: usersMock[2] });
      directive.ngOnInit();
      expect(clearSpy).toHaveBeenCalled();
    });

    it('should remove element when there is no user', () => {
      const clearSpy = spyOn(viewContainerRef, 'clear');
      _user.next({ data: null });
      directive.ngOnInit();
      expect(clearSpy).toHaveBeenCalled();
    });
  });

  describe('ngOnChanges', () => {
    it('should remove element when user has no authorization', () => {
      const clearSpy = spyOn(viewContainerRef, 'clear');
      const createEmbeddedViewSpy = spyOn(viewContainerRef, 'createEmbeddedView');
      directive.ngOnInit();
      directive.ngOnChanges();
      _user.next({ data: usersMock[2] });
      expect(clearSpy).toHaveBeenCalled();
      expect(createEmbeddedViewSpy).not.toHaveBeenCalled();
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe from user stream', () => {
      const clearSpy = spyOn(viewContainerRef, 'clear');
      const createEmbeddedViewSpy = spyOn(viewContainerRef, 'createEmbeddedView');
      directive.ngOnInit();
      directive.ngOnDestroy();
      _user.next({ data: null });
      expect(clearSpy).toHaveBeenCalledTimes(1);
      expect(createEmbeddedViewSpy).not.toHaveBeenCalled();
    });
  });
});

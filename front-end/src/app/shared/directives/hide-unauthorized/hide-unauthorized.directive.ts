import { Directive, OnDestroy, Input, OnChanges, ElementRef, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { Subscription } from 'rxjs';
import User from 'src/app/interfaces/user/user.interface';
import { ParentFeature, Feature } from 'src/app/interfaces/feature/feature.interface';
import { hasRequiredClaims } from 'src/app/helpers/has-required-claims';

@Directive({
  selector: '[hideUnauthorized]'
})
export class HideUnauthorizedDirective implements OnInit, OnChanges, OnDestroy {
  @Input() hideUnauthorized: ParentFeature | Feature;
  private _user: User;
  private _userSub: Subscription;

  constructor(
    private userService: UserService,
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
    this._userSub = this.userService.user$
      .subscribe(({ data }) => {
        this._user = data;
        this._setVisibility();
      });
  }

  ngOnChanges() {
    this._setVisibility();
  }

  ngOnDestroy() {
    this._userSub.unsubscribe();
  }

  private _setVisibility(): void {
    if (!this._user || !this.hideUnauthorized) {
      this.elementRef.nativeElement.classList.add('hidden');
      return;
    }
    const claims = hasRequiredClaims(this.hideUnauthorized.requiredClaims, this._user.claims);
    if (!claims.status) {
      this.elementRef.nativeElement.classList.add('hidden');
    } else {
      this.elementRef.nativeElement.classList.remove('hidden');
    }
  }
}

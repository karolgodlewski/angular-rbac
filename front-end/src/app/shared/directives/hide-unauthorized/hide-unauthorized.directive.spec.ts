import { async, TestBed } from '@angular/core/testing';
import { HideUnauthorizedDirective } from './hide-unauthorized.directive';
import { usersMock } from 'src/app/mocks/users.mock';
import { BehaviorSubject } from 'rxjs';
import { featuresMock } from 'src/app/mocks/features.mock';
import { ElementRef } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { featureNamesEnum } from 'src/app/enums/feature-names.enum';

const FakeElementRef = {
  nativeElement: {
    classList: {
      add() { },
      remove() { }
    }
  }
};

const _user = new BehaviorSubject({ data: usersMock[1] });
const FakeUserService = {
  user$: _user.asObservable(),
};

describe('HideUnauthorizedDirective', () => {
  let directive: HideUnauthorizedDirective;
  let elementRef: ElementRef;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        HideUnauthorizedDirective,
        { provide: ElementRef, useValue: FakeElementRef },
        { provide: UserService, useValue: FakeUserService }
      ]
    });
    directive = TestBed.get(HideUnauthorizedDirective);
    elementRef = TestBed.get(ElementRef);
  }));

  beforeEach(() => {
    const feature = featuresMock
      .find((f) => f.name === featureNamesEnum.HOME);
    directive.hideUnauthorized = feature.children
      .find((f) => f.name === featureNamesEnum.HOME__ADMIN_BTN);
    directive.ngOnInit();
  });

  describe('ngOnInit', () => {
    it('should remove hidden class when user is authorized', () => {
      const removeSpy = spyOn(elementRef.nativeElement.classList, 'remove');
      _user.next({ data: usersMock[1] });
      directive.ngOnChanges();
      expect(removeSpy).toHaveBeenCalledWith('hidden');
    });

    it('should add hidden class when user is not authorized', () => {
      const addSpy = spyOn(elementRef.nativeElement.classList, 'add');
      _user.next({ data: usersMock[2] });
      directive.ngOnChanges();
      expect(addSpy).toHaveBeenCalledWith('hidden');
    });

    it('should add hidden class when there is no user', () => {
      const addSpy = spyOn(elementRef.nativeElement.classList, 'add');
      _user.next({ data: null });
      directive.ngOnChanges();
      expect(addSpy).toHaveBeenCalledWith('hidden');
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe form user stream', () => {
      const removeSpy = spyOn(elementRef.nativeElement.classList, 'remove');
      _user.next({ data: usersMock[1] });
      directive.ngOnDestroy();
      _user.next({ data: usersMock[2] });
      expect(removeSpy).toHaveBeenCalledTimes(1);
    });
  });
});

import {
  Directive, Renderer2, OnChanges, ElementRef, OnInit, Input, OnDestroy
} from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { Subscription } from 'rxjs/internal/Subscription';
import User from 'src/app/interfaces/user/user.interface';
import { Feature } from 'src/app/interfaces/feature/feature.interface';
import { hasRequiredClaims } from 'src/app/helpers/has-required-claims';

@Directive({
  selector: '[disableUnauthorized]'
})
export class DisableUnauthorizedDirective implements OnInit, OnChanges, OnDestroy {
  @Input('disableUnauthorized') feature: Feature;
  private _user: User;
  private _userSub: Subscription;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private userService: UserService
  ) { }

  ngOnInit() {
    this._userSub = this.userService.user$
      .subscribe(({ data }: { data: User }): void => {
        this._user = data;
        this._toggleDisabled();
      });
  }

  ngOnChanges() {
    this._toggleDisabled();
  }

  ngOnDestroy() {
    this._userSub.unsubscribe();
  }

  private _toggleDisabled(): void {
    if (!this._user || !this.feature) {
      return;
    }
    const elem = this.elementRef.nativeElement;
    const claims = hasRequiredClaims(this.feature.requiredClaims, this._user.claims);
    !claims.status
      ? this.renderer.setAttribute(elem, 'disabled', 'true')
      : this.renderer.removeAttribute(elem, 'disabled');
  }
}

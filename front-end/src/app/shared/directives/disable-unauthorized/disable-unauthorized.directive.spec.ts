import { async, TestBed } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';
import { UserService } from 'src/app/core/services/user/user.service';
import { DisableUnauthorizedDirective } from './disable-unauthorized.directive';
import { ElementRef, Renderer2 } from '@angular/core';
import { featuresMock } from 'src/app/mocks/features.mock';
import { usersMock } from 'src/app/mocks/users.mock';
import { featureNamesEnum } from 'src/app/enums/feature-names.enum';


const FakeRenderer = {
  setAttribute() { },
  removeAttribute() { }
};

const FakeElementRef =  {
  nativeElement: { }
};

const _user = new BehaviorSubject({ data: usersMock[2] });
const FakeUserService = {
  user$: _user.asObservable(),
};

describe('DisableUnauthorizedDirective', () => {
  let directive: DisableUnauthorizedDirective;
  let elemRef;
  let renderer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        DisableUnauthorizedDirective,
        { provide: UserService, useValue: FakeUserService },
        { provide: ElementRef, useValue: FakeElementRef },
        { provide: Renderer2, useValue: FakeRenderer }
      ]
    });
    directive = TestBed.get(DisableUnauthorizedDirective);
    elemRef = TestBed.get(ElementRef);
    renderer = TestBed.get(Renderer2);
  }));

  beforeEach(() => {
    const feature = featuresMock
      .find((f) => f.name === featureNamesEnum.HOME);
    directive.feature = feature.children
      .find((f) => f.name === featureNamesEnum.HOME__ADMIN_BTN);
  });

  afterEach(() => {
    _user.next({ data: usersMock[2] });
  });

  describe('ngOnInit', () => {
    it('should set disabled atrribute when user not authorized', () => {
      const setAttributeSpy = jest.spyOn(renderer, 'setAttribute');
      directive.ngOnInit();
      expect(setAttributeSpy).toHaveBeenCalledWith(elemRef.nativeElement, 'disabled', 'true');
    });

    it('should remove disabled atrribute when user authorized', () => {
      const removeAttributeSpy = spyOn(renderer, 'removeAttribute');
      _user.next({ data: usersMock[1] });
      directive.ngOnInit();
      expect(removeAttributeSpy).toHaveBeenCalledWith(elemRef.nativeElement, 'disabled');
    });

    it('should return early when there is no user', () => {
      const setAttributeSpy = spyOn(renderer, 'setAttribute');
      const removeAttributeSpy = spyOn(renderer, 'removeAttribute');
      _user.next({ data: null });
      directive.ngOnInit();
      expect(setAttributeSpy).not.toHaveBeenCalled();
      expect(removeAttributeSpy).not.toHaveBeenCalled();
    });
  });

  describe('ngOnChanges', () => {
    it('should set disabled attribute when user not authorized', () => {
      const setAttributeSpy = spyOn(renderer, 'setAttribute');
      directive.ngOnInit();
      directive.ngOnChanges();
      expect(setAttributeSpy).toHaveBeenCalledWith(elemRef.nativeElement, 'disabled', 'true');
    });

    it('removes disabled attribute when user not authorized', () => {
      const removeAttributeSpy = spyOn(renderer, 'removeAttribute');
      _user.next({ data: usersMock[1] });
      directive.ngOnInit();
      expect(removeAttributeSpy).toHaveBeenCalledWith(elemRef.nativeElement, 'disabled');
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe from user stream', () => {
      const setAttributeSpy = spyOn(renderer, 'setAttribute');
      directive.ngOnInit();
      _user.next({ data: usersMock[2] });
      directive.ngOnDestroy();
      expect(setAttributeSpy).toHaveBeenCalledTimes(2);
    });
  });
});

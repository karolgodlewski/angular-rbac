import { async, TestBed } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';
import { IfAuthorizedPipe } from './if-authorized.pipe';
import { UserService } from 'src/app/core/services/user/user.service';
import { navItems } from '../components/left-nav/nav-items';
import { NavItem } from '../components/left-nav/let-nav-item/nav-item.interface';
import { usersMock } from 'src/app/mocks/users.mock';
import userRolesEnum from 'src/app/enums/user-roles.enum';

const _user = new BehaviorSubject({ data: usersMock[1] });
const FakeUserService = {
  user$: _user.asObservable(),
};

describe('IfAuthorizedPipe', () => {
  let pipe: IfAuthorizedPipe;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        IfAuthorizedPipe,
        { provide: UserService, useValue: FakeUserService }
      ]
    });
    pipe = TestBed.get(IfAuthorizedPipe);
    userService = TestBed.get(UserService);
  }));

  afterEach(() => {
    _user.next({ data: usersMock[1] });
  });

  describe('transform', () => {
    it('should return only those items that user has authorization to', () => {
      const ret = pipe.transform(navItems[2].children);
      expect(ret).toEqual([
        {
          displayName: 'Child 1',
          name: 'child_1',
          type: 'child',
          requiredClaims: []
        },
        {
          displayName: 'Child 2',
          name: 'child_2',
          type: 'child',
          requiredClaims: []
        },
        {
          displayName: 'Admin Child',
          name: 'child_4',
          type: 'child',
          requiredClaims: [
            'USE_NAV__ADMIN_CHILD_ITEM',
            'USE_NAV__SUPER_ADMIN_CHILD_ITEM'
          ]
        }
      ]);
    });

    it('should return null when there is no user', () => {
      _user.next({ data: null });
      const ret = pipe.transform(navItems[2].children);
      expect(ret).toBe(null);
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe from user stream', () => {
      _user.next({ data: usersMock[2] });
      pipe.ngOnDestroy();
      _user.next({ data: usersMock[0] });
      const transformSpy = jest.spyOn(pipe, 'transform');
      expect(transformSpy).not.toHaveBeenCalled();
    });
  });
});

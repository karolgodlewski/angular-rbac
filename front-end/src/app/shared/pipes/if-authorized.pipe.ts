import { Pipe, PipeTransform, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { Subscription } from 'rxjs';
import User from 'src/app/interfaces/user/user.interface';
import { hasRequiredClaims } from 'src/app/helpers/has-required-claims';

@Pipe({
  name: 'ifAuthorized',
  pure: false
})
export class IfAuthorizedPipe implements PipeTransform, OnDestroy {
  private _user: User;
  private _userSub: Subscription;

  constructor(private userService: UserService) {
    this._userSub = this.userService.user$
      .subscribe(({ data }) => {
        this._user = data;
      });
  }

  ngOnDestroy() {
    this._userSub.unsubscribe();
  }

  transform(value: any[]): any {
    if (!this._user) {
      return null;
    }
    return value.filter((v) => {
      const claims = hasRequiredClaims(v.requiredClaims, this._user.claims);
      if (claims.status) {
        return v;
      }
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftNavComponent } from './components/left-nav/left-nav.component';
import { ShellComponent } from './components/shell/shell.component';
import { LeftNavItemComponent } from './components/left-nav/let-nav-item/left-nav-item.component';
import { IfAuthorizedDirective } from './directives/if-authorized.directive';
import { IfAuthorizedPipe } from './pipes/if-authorized.pipe';
import { HideUnauthorizedDirective } from './directives/hide-unauthorized/hide-unauthorized.directive';
import { DisableUnauthorizedDirective } from './directives/disable-unauthorized/disable-unauthorized.directive';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';



@NgModule({
  declarations: [
    LeftNavComponent,
    ShellComponent,
    LeftNavItemComponent,
    IfAuthorizedDirective,
    IfAuthorizedPipe,
    HideUnauthorizedDirective,
    DisableUnauthorizedDirective,
    AccessDeniedComponent,
    TopNavComponent,
    MaintenanceComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LeftNavComponent,
    ShellComponent,
    IfAuthorizedDirective,
    IfAuthorizedPipe,
    HideUnauthorizedDirective,
    DisableUnauthorizedDirective,
    TopNavComponent
  ]
})
export class SharedModule { }

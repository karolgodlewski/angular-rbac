import { Component } from '@angular/core';

@Component({
  selector: 'app-shell',
  template: `
    <div class="shell">
      <ng-content></ng-content>
    </div>
  `,
  styles: [`
    :host {
      display: block;
      height: 100%;
    }

    .shell {
      height: 100%;
    }
  `]
})
export class ShellComponent { }

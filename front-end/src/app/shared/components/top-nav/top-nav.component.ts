import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Observable, Subscription } from 'rxjs';
import { UserService } from 'src/app/core/services/user/user.service';
import User from 'src/app/interfaces/user/user.interface';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit, OnDestroy {
  authorized$: Observable<boolean>;
  user$: Observable<{ data: User }>;
  private _logoutSub: Subscription;

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.authorized$ = this.authService.authorized$;
    this.user$ = this.userService.user$;
  }

  ngOnDestroy() {
    if (this._logoutSub) {
      this._logoutSub.unsubscribe();
    }
  }

  onLogoutClick() {
    this._logoutSub = this.authService.logout().subscribe();
  }
}

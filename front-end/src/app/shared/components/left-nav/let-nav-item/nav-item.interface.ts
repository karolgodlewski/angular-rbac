export interface NavItem {
  displayName: string;
  name: string;
  type: 'master' | 'child' | 'group';
  requiredClaims: string[];
  children?: NavItem[];
  path?: string;
}

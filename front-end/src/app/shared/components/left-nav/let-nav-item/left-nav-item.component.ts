import { Component, Input } from '@angular/core';
import { NavItem } from './nav-item.interface';

@Component({
  selector: 'app-left-nav-item',
  templateUrl: './left-nav-item.component.html',
  styleUrls: ['./left-nav-item.component.scss']
})
export class LeftNavItemComponent {
  @Input() navItem: NavItem;
}

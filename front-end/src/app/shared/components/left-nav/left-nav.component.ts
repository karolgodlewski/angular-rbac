import { Component, OnInit } from '@angular/core';
import { NavItem } from './let-nav-item/nav-item.interface';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { navItems } from './nav-items';

@Component({
  selector: 'app-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.scss']
})
export class LeftNavComponent implements OnInit {
  navItems: NavItem[] = navItems;
  authorized$: Observable<boolean>;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authorized$ = this.authService.authorized$;
  }
}

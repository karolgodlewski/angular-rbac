import { NavItem } from './let-nav-item/nav-item.interface';

export const navItems: NavItem[] = [
  {
    displayName: 'Item',
    name: 'item',
    type: 'master',
    requiredClaims: [ 'ITEM_1' ]
  },
  {
    displayName: 'AdminItem',
    name: 'admin_item',
    type: 'master',
    requiredClaims: [
      'USE_NAV__ADMIN_ITEM',
      'USE_NAV__SUPER_ADMIN_ITEM'
    ]
  },
  {
    displayName: 'GroupItem',
    name: 'group_item',
    type: 'group',
    requiredClaims: [],
    children: [
      {
        displayName: 'Child 1',
        name: 'child_1',
        type: 'child',
        requiredClaims: []
      },
      {
        displayName: 'Child 2',
        name: 'child_2',
        type: 'child',
        requiredClaims: []
      },
      {
        displayName: 'SuperAdmin Child',
        name: 'child_3',
        type: 'child',
        requiredClaims: ['USE_NAV__SUPER_ADMIN_CHILD_ITEM']
      },
      {
        displayName: 'Admin Child',
        name: 'child_4',
        type: 'child',
        requiredClaims: [
          'USE_NAV__ADMIN_CHILD_ITEM',
          'USE_NAV__SUPER_ADMIN_CHILD_ITEM'
        ]
      },
      {
        displayName: 'Regular Child',
        name: 'child_5',
        type: 'child',
        requiredClaims: ['USE_NAV__REGULAR_CHILD_ITEM']
      }
    ]
  }
];

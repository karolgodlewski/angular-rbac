import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './core/services/user/user.service';

@Component({
  selector: 'app-left-nav',
  template: `
    <div></div>
  `
})
export class FakeLeftNavComponent { }

@Component({
  selector: 'app-top-nav',
  template: `
    <div></div>
  `
})
export class FakeTopNavComponent { }

@Component({
  selector: 'app-shell',
  template: `
    <div></div>
  `
})
export class FakeShellComponent { }

const _user = new BehaviorSubject({ data: null });
const userServiceMock = {
  user$: _user.asObservable()
};

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        FakeLeftNavComponent,
        FakeTopNavComponent,
        FakeShellComponent
      ],
      providers: [
        {
          provide: UserService,
          useValue: userServiceMock
        }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  }));

  beforeEach(() => {
    component.ngOnInit();
  });

  describe('ngOnInit', () => {
    it('should subscribe to user', () => {
      expect(component.userSub).toBeTruthy();
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe from user stream', () => {
      component.ngOnDestroy();
      expect(component.userSub.closed).toBeTruthy();
    });
  });
});

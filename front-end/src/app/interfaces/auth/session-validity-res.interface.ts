import User from '../user/user.interface';

export default interface SessionValidityRes {
  session: boolean;
  user: User;
}

import userRolesEnum from 'src/app/enums/user-roles.enum';

export default interface User {
  readonly _id: string;
  readonly username: string;
  readonly password: string;
  readonly role: userRolesEnum.SUPER_ADMIN | userRolesEnum.ADMIN | userRolesEnum.REGULAR;
  readonly claims: string[];
}

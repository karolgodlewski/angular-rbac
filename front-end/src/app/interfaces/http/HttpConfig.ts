import { environment } from '../../../environments/environment';

export class HttpConfig {
  apiUrl; auth; queryParams;

  constructor(o) {
    this.apiUrl = o.apiUrl || environment.API_URL;
    this.auth = typeof o.auth !== 'undefined' ? o.auth : true;
    this.queryParams = o.queryParams || {};
  }
}

export interface IHttpReqOptionalConfig {
  auth?: boolean;
  queryParams?: { [key: string]: any };
}

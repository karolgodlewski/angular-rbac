import { hasRequiredClaims } from './has-required-claims';

describe('hasRequiredClaims', () => {
  it('should return common keys', () => {
    const ret = hasRequiredClaims(['a', 'c'], ['a', 'b', 'c']);
    expect(ret).toEqual({ status: true, commonClaims: ['a', 'c'] });
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { AuthResolver } from '../core/resolvers/auth/auth.resolver';
import { featureNamesEnum } from '../enums/feature-names.enum';
import { appRoutesEmum } from '../enums/app-routes.enum';


const routes: Routes = [
  {
    path: appRoutesEmum.ADMIN_PANEL,
    component: AdminPanelComponent,
    canActivate: [AuthGuard],
    data: { featureName: featureNamesEnum.ADMIN_PANEL },
    resolve: {
      featureData: AuthResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPanelRoutingModule { }

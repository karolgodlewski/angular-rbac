import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './core/services/user/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
    :host {
      display: block;
      height: 100%;
    }

    .app {
      display: flex;
      flex: 1;
      height: 100%;
    }

    .app__mid-col {
      display: flex;
      flex-direction: column;
      flex: 1;
    }

    .app__view-content {
      margin-top: 15px;
      flex: 1;
    }
  `]
})
export class AppComponent implements OnInit, OnDestroy {
  userSub: Subscription;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userSub = this.userService.user$.subscribe();
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessDeniedComponent } from './shared/components/access-denied/access-denied.component';
import { AuthGuard } from './core/guards/auth.guard';
import { MaintenanceComponent } from './shared/components/maintenance/maintenance.component';
import { appRoutesEmum } from './enums/app-routes.enum';


const routes: Routes = [
  {
    path: appRoutesEmum.ACCESS_DENIED,
    component: AccessDeniedComponent,
    canActivate: [AuthGuard]
  },
  {
    path: appRoutesEmum.MAINTENANCE,
    component: MaintenanceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

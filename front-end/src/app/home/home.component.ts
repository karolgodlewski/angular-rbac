import { Component, OnInit, OnDestroy } from '@angular/core';
import userRolesEnum from '../enums/user-roles.enum';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../core/services/auth/auth.service';
import { Subscription } from 'rxjs';
import { ParentFeature, Feature } from '../interfaces/feature/feature.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  userRoles = userRolesEnum;
  featureData: ParentFeature;
  private _sub: Subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.featureData = this.route.snapshot.data.featureData;
  }

  ngOnDestroy() {
    if (this._sub) {
      this._sub.unsubscribe();
    }
  }

  getChildFeature(featureName: string): Feature {
    return this.featureData.children
      .find((c) => c.name === featureName);
  }

  onSetUserClick(role: string) {
    let sub;
    switch (role) {
      case userRolesEnum.SUPER_ADMIN:
        sub = this.authService.login({ username: userRolesEnum.SUPER_ADMIN, password: 'pass'})
          .subscribe();
        this._sub.add(sub);
        break;
      case userRolesEnum.ADMIN:
        sub = this.authService.login({ username: userRolesEnum.ADMIN, password: 'pass'})
          .subscribe();
        this._sub.add(sub);
        break;
      case userRolesEnum.REGULAR:
        sub = this.authService.login({ username: userRolesEnum.REGULAR, password: 'pass'})
          .subscribe();
        this._sub.add(sub);
        break;
      default:
        console.log('HomeComponent:role_not_specified');
        break;
    }
  }

  on401ResponseClick(): void {
    const sub = this.authService.trig401Response().subscribe();
    this._sub.add(sub);
  }
}

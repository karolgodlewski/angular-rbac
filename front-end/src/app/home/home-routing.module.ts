import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { AuthResolver } from '../core/resolvers/auth/auth.resolver';
import { featureNamesEnum } from '../enums/feature-names.enum';
import { appRoutesEmum } from '../enums/app-routes.enum';


const routes: Routes = [
  {
    path: appRoutesEmum.HOME,
    component: HomeComponent,
    canActivate: [AuthGuard],
    resolve: {
      featureData: AuthResolver
    },
    data: { featureName: featureNamesEnum.HOME },
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }

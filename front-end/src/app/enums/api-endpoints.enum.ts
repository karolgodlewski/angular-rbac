export enum apiEndpointsEnum {
  LOGIN = 'login',
  LOGOUT = 'logout',
  USERS = 'users',
  AUTH_DATA = 'feature-availability',
  RANDOM_ENDPOINT = 'random-endpoint'
}

export enum authEnum {
  AUTH_HEADER = 'Authorization',
  AUTH_TOKEN_LOCAL_STORAGE_KEY = 'authToken',
}

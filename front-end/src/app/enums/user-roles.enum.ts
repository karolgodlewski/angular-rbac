enum userRolesEnum {
  SUPER_ADMIN = 'SUPER_ADMIN',
  ADMIN = 'ADMIN',
  REGULAR = 'REGULAR'
}

export default userRolesEnum;

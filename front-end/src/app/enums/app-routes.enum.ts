export enum appRoutesEmum {
  HOME = '',
  ACCESS_DENIED = 'access-denied',
  ADMIN_PANEL = 'admin-panel',
  LOGIN = 'login',
  USERS = 'users',
  MAINTENANCE = 'maintenance'
}

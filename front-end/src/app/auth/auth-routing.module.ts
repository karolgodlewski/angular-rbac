import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { appRoutesEmum } from '../enums/app-routes.enum';


const routes: Routes = [
  { path: appRoutesEmum.LOGIN, component: AuthComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }

import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnDestroy {
  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
  loginSub: Subscription;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnDestroy() {
    this.loginSub.unsubscribe();
  }

  onFormSubmit(e) {
    e.preventDefault();
    this.loginSub = this.authService.login({
      username: this.loginForm.value.username.toUpperCase(),
      password: this.loginForm.value.password
    })
      .subscribe(() => this.router.navigate(['/']));
  }
}

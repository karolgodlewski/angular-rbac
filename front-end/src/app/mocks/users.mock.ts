import User from 'src/app/interfaces/user/user.interface';
import userRolesEnum from '../enums/user-roles.enum';

export const usersMock: User[] = [
  {
    _id: 'ysai7t8sat86ra',
    username: userRolesEnum.SUPER_ADMIN,
    password: 'pass',
    role: userRolesEnum.SUPER_ADMIN,
    claims: [
      'VIEW_HOME',
      'VIEW_ADMIN_PANEL',
      'USE_HOME__SUPER_ADMIN_BTN',
      'USE_NAV__SUPER_ADMIN_ITEM',
      'USE_NAV__SUPER_ADMIN_CHILD_ITEM'
    ]
  },
  {
    _id: '097knfut6fegkjs',
    username: userRolesEnum.ADMIN,
    password: 'pass',
    role: userRolesEnum.ADMIN,
    claims: [
      'VIEW_HOME',
      'VIEW_ADMIN_PANEL',
      'USE_HOME__ADMIN_BTN',
      'USE_NAV__ADMIN_ITEM',
      'USE_NAV__ADMIN_CHILD_ITEM'
    ]
  },
  {
    _id: '99jlkniut78tdsgh',
    username: userRolesEnum.REGULAR,
    password: 'pass',
    role: userRolesEnum.REGULAR,
    claims: [
      'VIEW_HOME',
      'USE_NAV__REGULAR_CHILD_ITEM'
    ]
  }
];

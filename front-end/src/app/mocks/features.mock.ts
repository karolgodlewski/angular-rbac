import { featureNamesEnum } from '../enums/feature-names.enum';
import { ParentFeature } from '../interfaces/feature/feature.interface';

export const featuresMock: ParentFeature[] = [
  {
    _id: '98yoihfoihsus',
    name: featureNamesEnum.APP,
    status: 0,
    statusText: 'MAINTENANCE',
    requiredClaims: [],
    children: [
      {
        _id: 'dsg087ihkjsgdji',
        name: featureNamesEnum.APP__TEST,
        status: 1,
        statusText: 'ENABLED',
        requiredClaims: []
      }
    ]
  },
  {
    _id: '9ufhjdsgrfot4e',
    name: featureNamesEnum.HOME,
    status: 1,
    statusText: 'ENABLED',
    requiredClaims: [
      'VIEW_HOME'
    ],
    children: [
      {
        _id: 'hp7askbcx98io',
        name: featureNamesEnum.HOME__SUPER_ADMIN_BTN,
        status: 1,
        statusText: 'ENABLED',
        requiredClaims: [
          'USE_HOME__SUPER_ADMIN_BTN'
        ]
      },
      {
        _id: 'asz879uoijlknds',
        name: featureNamesEnum.HOME__ADMIN_BTN,
        status: 1,
        statusText: 'ENABLED',
        requiredClaims: [
          'USE_HOME__SUPER_ADMIN_BTN',
          'USE_HOME__ADMIN_BTN'
        ]
      },
      {
        _id: '09hds5rfdsjfgjbv',
        name: featureNamesEnum.HOME__REGULAR_BTN,
        status: 1,
        statusText: 'ENABLED',
        requiredClaims: []
      }
    ]
  },
  {
    _id: '132rfdsgdfdgd',
    name: featureNamesEnum.ADMIN_PANEL,
    status: 1,
    statusText: 'ENABLED',
    requiredClaims: [
      'VIEW_ADMIN_PANEL'
    ],
    children: []
  }
];

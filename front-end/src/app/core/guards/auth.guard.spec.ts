import { async, TestBed } from '@angular/core/testing';
import { Subscription, of, throwError } from 'rxjs';
import { AuthGuard } from './auth.guard';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { UserService } from '../services/user/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { usersMock } from 'src/app/mocks/users.mock';

const FakeAuthService = {
  redirectTo: ['/'],
  redirectBack: false,
  getSessionValidity() {},
  getRedirectToPath: () => {
    return FakeAuthService.redirectTo[ FakeAuthService.redirectTo.length - 1 ];
  },
  setRedirectToPath(path: string) {
    this.redirectTo = [
      this.getRedirectToPath(),
      path
    ];
  },
  logout: jest.fn()
};

const FakeUserService = {
  setUser() { }
};

const FakeRouter = {
  navigate(d) { }
};

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let sub: Subscription;
  let authService;
  let router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Router, useValue: FakeRouter },
        { provide: AuthService, useValue: FakeAuthService },
        { provide: UserService, useValue: FakeUserService }
      ]
    });
    guard = TestBed.get(AuthGuard);
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
  }));

  afterEach(() => {
    authService.redirectTo = ['/'];
    authService.redirectBack = false;
    if (sub) {
      sub.unsubscribe();
    }
  });

  describe('canActivate', () => {
    it('should navigate back to previous route when /login route attempt made and already authenticated', (done) => {
      const navigateSpy = jest.spyOn(router, 'navigate');
      spyOn(authService, 'getSessionValidity').and.returnValue(of({
        session: true,
        user: usersMock[1]
      }));
      sub = guard.canActivate({} as any, { url: '/login' } as any)
        .subscribe((data) => {
          expect(authService.redirectBack).toBe(false);
          expect(navigateSpy).toHaveBeenCalledTimes(1);
          expect(navigateSpy).toHaveBeenCalledWith(['/']);
          expect(data).toBe(false);
          done();
        });
    });

    it('should navigate back to redirect target when /login route attempt made and redirectBack set to true', (done) => {
      const navigateSpy = jest.spyOn(router, 'navigate');
      spyOn(authService, 'getSessionValidity').and.returnValue(of({
        session: true,
        user: usersMock[1]
      }));
      spyOn(authService, 'getRedirectToPath').and.returnValue('/admin-panel');
      authService.redirectBack = true;
      sub = guard.canActivate({} as any, { url: '/any-page' } as any)
        .subscribe((data) => {
          expect(authService.redirectBack).toBe(false);
          expect(navigateSpy).toHaveBeenCalledTimes(1);
          expect(navigateSpy).toHaveBeenCalledWith(['/admin-panel']);
          expect(data).toBe(false);
          done();
        });
    });

    it('should set redirectTo path when redirectBack flag set to false', (done) => {
      const stateUrl = '/any-page';
      const setRedirectToPathSpy = spyOn(authService, 'setRedirectToPath');
      spyOn(authService, 'getSessionValidity').and.returnValue(of({
        session: true,
        user: usersMock[1]
      }));
      authService.redirectBack = false;
      sub = guard.canActivate({} as any, { url: stateUrl } as any)
        .subscribe((data) => {
          expect(!authService.redirectBack).toBe(true);
          expect(setRedirectToPathSpy).toHaveBeenCalledWith(stateUrl);
          expect(data).toBe(true);
          done();
        });
    });

    it('should stay on login page when http error has been thrown for /login route', (done) => {
      spyOn(authService, 'getSessionValidity').and.returnValue(throwError(new HttpErrorResponse({ error: true })));
      sub = guard.canActivate({} as any, { url: '/login' } as any)
        .subscribe((err) => {
          expect(authService.redirectBack).toBe(false);
          expect(err).toBe(true);
          done();
        });
    });

    it('should throw an error when http error has been thrown and attempted routed differs from /login', (done) => {
      const error = new HttpErrorResponse({ error: 'Invalid user session' });
      const setRedirectToPathSpy = spyOn(authService, 'setRedirectToPath');
      const logoutSpy = jest.spyOn(authService, 'logout').mockReturnValue(of(true));
      const stateUrl = '/admin-panel';
      spyOn(authService, 'getSessionValidity').and.returnValue(throwError(error));
      sub = guard.canActivate({} as any, { url: stateUrl } as any)
        .pipe(
          catchError((err) => {
            expect(authService.redirectBack).toBe(true);
            expect(setRedirectToPathSpy).toHaveBeenCalledTimes(1);
            expect(setRedirectToPathSpy).toHaveBeenCalledWith(stateUrl);
            expect(logoutSpy).toHaveBeenCalledTimes(1);
            expect(err).toEqual(error);
            done();
            return of(err);
          })
        )
        .subscribe();
    });
  });
});

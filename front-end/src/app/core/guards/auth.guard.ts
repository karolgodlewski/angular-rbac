import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';
import { map, catchError } from 'rxjs/operators';
import { appRoutesEmum } from 'src/app/enums/app-routes.enum';
import { UserService } from '../services/user/user.service';
import SessionValidityRes from 'src/app/interfaces/auth/session-validity-res.interface';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const isLogin = state.url === '/' + appRoutesEmum.LOGIN;
    return this.authService.getSessionValidity()
      .pipe(
        map((res: SessionValidityRes): boolean => {
          this.userService.setUser(res.user);
          /*
           * User session validation returned success and the user tried to access login
           * view. There is no valid reason to authenticate again so the app redirects
           * back to previous route.
           */
          if (isLogin) {
            this.router.navigate([this.authService.getRedirectToPath()]);
            return false;
          }
          // FIXME: invalid condition?
          /*
          * User session validation returned success so the client has valid claim
          * to enter restricted route. By default redirect points to home screen.
          * In scenario when user tried to access route without valid token the
          * app navigates to login screen and saves the path. After successful
          * authentication the user is redirected back to the route saved on
          * the previous attempt.
          */
          if (this.authService.redirectBack) {
            this.authService.redirectBack = false;
            this.router.navigate([this.authService.getRedirectToPath()]);
            return false;
          }
          /*
           * Save currently attempted route for redirection purposes.
           * When reditectBack flag is set to true the user has been already
           * redirected to /login route. After successful authentication
           * redirectBack flag is set to false - there is no reason to
           * do another redirect.
           */
          if (!this.authService.redirectBack) {
            this.authService.setRedirectToPath(state.url);
          }
          return true;
        }),
        catchError((err: HttpErrorResponse): Observable<boolean | never> => {
          if (isLogin) {
            // stay on /login
            return of(true);
          }
          // go to /login when session validation failed
          this.authService.redirectBack = true;
          this.authService.setRedirectToPath(state.url);
          this.authService.logout().subscribe();
          return throwError(err);
        })
      );
  }
}

import { Subscription, of, BehaviorSubject, throwError } from 'rxjs';
import { TestBed, async } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { UserService } from '../../services/user/user.service';
import { AuthResolver } from './auth.resolver';
import { featuresMock } from 'src/app/mocks/features.mock';
import { appRoutesEmum } from 'src/app/enums/app-routes.enum';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { usersMock } from 'src/app/mocks/users.mock';
import { featureNamesEnum } from 'src/app/enums/feature-names.enum';

const _authorized = new BehaviorSubject(false);
const FakeAuthService = {
  authorized: _authorized.asObservable(),
  redirectTo: ['/'],
  redirectBack: false,
  getFeatureData() { },
  setAuthorizedStatus(status) {
    _authorized.next(status);
  }
};

const _user = new BehaviorSubject({ data: usersMock[1] });
const FakeUserService = {
  user$: _user.asObservable(),
  setUser() { }
};

const FakeRouter = {
  navigate() { }
};

describe('AuthResolver', () => {
  let resolver: AuthResolver;
  let sub: Subscription;
  let authService;
  let router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Router, useValue: FakeRouter },
        { provide: AuthService, useValue: FakeAuthService },
        { provide: UserService, useValue: FakeUserService }
      ]
    });
    resolver = TestBed.get(AuthResolver);
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
  }));

  afterEach(() => {
    _user.next({ data: usersMock[1] });
    if (sub) {
      sub.unsubscribe();
    }
  });

  describe('resolve', () => {
    it('should resolve feature auth data when user has been already authorized', (done) => {
      const featureName = featureNamesEnum.HOME;
      const routeData = { featureName };
      const featureData = {
        [featureName]: featuresMock.find((f) => f.name === featureName)
      };
      spyOn(authService, 'getFeatureData').and.returnValue(of(featureData));
      sub = resolver.resolve({ data: routeData } as any)
        .subscribe((res) => {
          expect(res).toEqual(featureData[featureName]);
          done();
        });
    });

    it('should navigate to /access-denied route when user has not been authorized yet', (done) => {
      const featureName = featureNamesEnum.ADMIN_PANEL;
      const routeData = { featureName };
      const featureData = {
        [featureName]: featuresMock.find((f) => f.name === featureName)
      };
      _user.next({ data: usersMock[2] });
      spyOn(authService, 'getFeatureData').and.returnValue(of(featureData));
      const navitateSpy = spyOn(router, 'navigate');
      sub = resolver.resolve({ data: routeData } as any)
        .subscribe((res) => {
          expect(res).not.toBeDefined();
          expect(navitateSpy).toHaveBeenCalledWith([appRoutesEmum.ACCESS_DENIED]);
          done();
        });
    });

    it('shoul rethrow error', (done) => {
      const featureName = 'HOME';
      const routeData = { featureName };
      const error = new HttpErrorResponse({ error: 'Http req error' });
      spyOn(authService, 'getFeatureData').and.returnValue(throwError(error));
      sub = resolver.resolve({ data: routeData } as any)
        .pipe(
          catchError((err) => {
            expect(err).toEqual(error);
            done();
            return of(err);
          })
        )
        .subscribe();
    });
  });
});

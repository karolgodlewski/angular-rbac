import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, Resolve } from '@angular/router';
import { combineLatest, Observable, throwError } from 'rxjs';
import { map, catchError, take } from 'rxjs/operators';
import { AuthService } from '../../services/auth/auth.service';
import { UserService } from '../../services/user/user.service';
import User from 'src/app/interfaces/user/user.interface';
import { appRoutesEmum } from 'src/app/enums/app-routes.enum';
import { HttpErrorResponse } from '@angular/common/http';
import { ParentFeature } from 'src/app/interfaces/feature/feature.interface';
import { hasRequiredClaims } from 'src/app/helpers/has-required-claims';


@Injectable({
  providedIn: 'root'
})
export class AuthResolver implements Resolve<ParentFeature> {
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ParentFeature> {
    const { featureName } = route.data;
    return combineLatest([
      this.authService.getFeatureData(featureName),
      this.userService.user$
    ])
      .pipe(
        map(([ featureData, user ]: [ { [ key: string ]: ParentFeature}, { data: User } ]): ParentFeature => {
          const claims = hasRequiredClaims(featureData[featureName].requiredClaims, user.data.claims);
          this.authService.setAuthorizedStatus(true);
          if (!claims.status) {
            this.router.navigate([appRoutesEmum.ACCESS_DENIED]);
            return;
          }
          return featureData[featureName];
        }),
        take(1),
        catchError((err: HttpErrorResponse): Observable<never> => {
          return throwError(err);
        })
      );
  }
}

import { TestBed, async } from '@angular/core/testing';
import { FakeBackend } from './fake-backend.interceptor';
import { HttpRequest, HttpResponse, HttpErrorResponse, HttpHeaders, HttpHandler } from '@angular/common/http';
import { of, Observable, Subscription } from 'rxjs';
import { apiEndpointsEnum } from 'src/app/enums/api-endpoints.enum';
import { usersMock } from 'src/app/mocks/users.mock';
import { catchError } from 'rxjs/operators';
import { featuresMock } from 'src/app/mocks/features.mock';


class FakeHttpHandler implements HttpHandler {
  handle(req) {
    return of(req);
  }
}

describe('FakeBackendInterceptor', () => {
  let resolver: FakeBackend;
  let sub: Subscription;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        FakeBackend
      ]
    });
    resolver = TestBed.get(FakeBackend);
  }));

  afterEach(() => {
    if (sub) {
      sub.unsubscribe();
    }
  });

  describe('intercept', () => {
    it('should emit 200 success res when intercepts /login POST req with valid user data as body', (done) => {
      const req = new HttpRequest('POST', apiEndpointsEnum.LOGIN, {
        username: usersMock[1].username,
        password: usersMock[1].password
      });
      resolver.intercept(req, new FakeHttpHandler())
        .subscribe((res: HttpResponse<any>) => {
          expect(res.body.user).toEqual(usersMock[1]);
          expect(res.status).toBe(200);
          expect(res.statusText).toBe('OK');
          done();
        });
    });

    it('should emit 401 error res when intercepts /login POST req with invalid user data as body', (done) => {
      const req = new HttpRequest('POST', apiEndpointsEnum.LOGIN, {
        username: usersMock[1].username,
        password: 'invalid-password'
      });
      sub = resolver.intercept(req, new FakeHttpHandler())
        .pipe(
          catchError((err: HttpErrorResponse): Observable<true> => {
            expect(err.error).toEqual({ errorMessage: 'unauthorized' });
            expect(err.status).toBe(401);
            expect(err.statusText).toBe('ERROR');
            done();
            return of(true);
          })
        )
        .subscribe();
    });

    it('should emit 200 success res when intercepts /login GET req with token contained in Authorization header', (done) => {
      const req = new HttpRequest('GET', apiEndpointsEnum.LOGIN, {
        headers: new HttpHeaders({
          Authorization: `Bearer ${usersMock[1]._id}`
        })
      });
      sub = resolver.intercept(req, new FakeHttpHandler())
        .subscribe((res: HttpResponse<any>) => {
          expect(res.body).toEqual({
            session: true,
            user: usersMock[1]
          });
          expect(res.status).toBe(200);
          expect(res.statusText).toBe('OK');
          done();
        });
    });

    it('should emit 401 error res when intercepts /login GET req with invalid Authorization header', (done) => {
      const req = new HttpRequest('GET', apiEndpointsEnum.LOGIN, {
        headers: new HttpHeaders({
          Authorization: `Token abcsfd`
        })
      });
      sub = resolver.intercept(req, new FakeHttpHandler())
        .pipe(
          catchError((err: HttpErrorResponse): Observable<true> => {
            expect(err.error).toEqual({ errorMessage: 'unauthorized' });
            expect(err.status).toBe(401);
            expect(err.statusText).toBe('ERROR');
            done();
            return of(true);
          })
        )
        .subscribe();
    });

    it('should emit 200 success res when intercepts /auth-data GET req with correct feature name as query param', (done) => {
      const req = new HttpRequest('GET', `${apiEndpointsEnum.AUTH_DATA}?target=${Object.keys(featuresMock[0])[0]}`, {
        headers: new HttpHeaders({
          Authorization: `Bearer ${usersMock[1]._id}`
        })
      });
      sub = resolver.intercept(req, new FakeHttpHandler())
        .subscribe((res: HttpResponse<any>) => {
          expect(res.body).toEqual({
            featureData: featuresMock[Object.keys(featuresMock[0])[0]]
          });
          expect(res.status).toBe(200);
          expect(res.statusText).toBe('OK');
          done();
        });
    });

    it('should emit 401 error res when intercepts /auth-data GET req with invalid token contained in Authorization header', (done) => {
      const req = new HttpRequest('GET', `${apiEndpointsEnum.AUTH_DATA}?target=${Object.keys(featuresMock)[0]}`, {
        headers: new HttpHeaders({
          Authorization: `Bearer abcsfd`
        })
      });
      sub = resolver.intercept(req, new FakeHttpHandler())
        .pipe(
          catchError((err: HttpErrorResponse): Observable<true> => {
            expect(err.error).toEqual({ errorMessage: 'unauthorized' });
            expect(err.status).toBe(401);
            expect(err.statusText).toBe('ERROR');
            done();
            return of(true);
          })
        )
        .subscribe();
    });

    it('should emit 200 success res when intercepts /random-endpoint GET req', (done) => {
      const req = new HttpRequest('GET', apiEndpointsEnum.RANDOM_ENDPOINT, {
        headers: new HttpHeaders({
          Authorization: `Bearer ${usersMock[1]._id}`
        })
      });
      sub = resolver.intercept(req, new FakeHttpHandler())
        .subscribe((res: HttpResponse<any>) => {
          expect(res.body).toEqual({
            data: true
          });
          expect(res.status).toBe(200);
          expect(res.statusText).toBe('OK');
          done();
        });
    });

    it('should emit 401 error res when intercepts /auth-data GET req without Authorization header', (done) => {
      const req = new HttpRequest('GET', apiEndpointsEnum.RANDOM_ENDPOINT);
      sub = resolver.intercept(req, new FakeHttpHandler())
        .pipe(
          catchError((err: HttpErrorResponse): Observable<true> => {
            expect(err.error).toEqual({ errorMessage: 'unauthorized' });
            expect(err.status).toBe(401);
            expect(err.statusText).toBe('ERROR');
            done();
            return of(true);
          })
        )
        .subscribe();
    });

    it('should call next.handle when no handler match for endpoint', (done) => {
      const req = new HttpRequest('GET', 'abcd');
      const fakeHttpHandler = new FakeHttpHandler();
      const fakeHttpHandlerSpy = jest.spyOn(fakeHttpHandler, 'handle');
      sub = resolver.intercept(req, fakeHttpHandler)
        .subscribe((res: HttpResponse<any>) => {
          expect(res.url).toBe('abcd');
          expect(fakeHttpHandlerSpy).toHaveBeenCalledTimes(1);
          expect(fakeHttpHandlerSpy).toHaveBeenCalledWith(res);
          done();
        });
    });
  });
});

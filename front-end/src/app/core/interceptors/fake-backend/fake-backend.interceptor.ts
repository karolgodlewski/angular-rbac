import { Injectable } from '@angular/core';
import {
  HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse, HttpHeaders, HttpEvent
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { mergeMap, delay } from 'rxjs/operators';
import { authEnum } from 'src/app/enums/auth.enum';
import { usersMock } from '../../../mocks/users.mock';
import { featuresMock } from '../../../mocks/features.mock';
import { apiEndpointsEnum } from 'src/app/enums/api-endpoints.enum';

@Injectable()
export class FakeBackend implements HttpInterceptor {
  private _TOKEN_BASE = 'Bearer ';

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const authHeader = req.headers.get(authEnum.AUTH_HEADER);
    const authHeaderValid = authHeader && authHeader.startsWith(this._TOKEN_BASE);
    return of(null)
      .pipe(
        mergeMap((): Observable<HttpResponse<any> | HttpErrorResponse | HttpEvent<any>> => {
          const { url, method, body } = req;
          const isGet = method === 'GET';
          const isPost = method === 'POST';
          const authToken = authHeaderValid && authHeader.split(' ')[1];
          const tokenValid = authToken && authToken !== 'null';

          if (url.startsWith(apiEndpointsEnum.LOGIN) && isPost) {
            const user = usersMock
              .find((u) => u.username === body.username && u.password === body.password);
            if (!user) {
              return this._unauthorized();
            }
            return this._success({ user });
          }
          if (url.startsWith(apiEndpointsEnum.LOGIN) && isGet) {
            const user = usersMock.find((u) => u._id === authToken);
            if (!tokenValid || !user) {
              return this._unauthorized();
            }
            return this._success({ session: true, user });
          }
          if (req.url.includes(apiEndpointsEnum.AUTH_DATA) && isGet) {
            const user = usersMock.find((u) => u._id === authToken);
            if (!tokenValid || !user) {
              return this._unauthorized();
            }
            const target = req.url.split('?')[1].split('=')[1];
            const featureData = featuresMock.find((f) => f.name === target);
            return this._success({ featureData , token: authToken });
          }
          if (req.url.includes(apiEndpointsEnum.RANDOM_ENDPOINT) && isGet) {
            if (!tokenValid) {
              return this._unauthorized();
            }
            return this._success({ data: true , token: authToken });
          }
          return next.handle(req);
        }),
        delay(500)
      );
  }

  private _success(data: any): Observable<HttpResponse<any>> {
    const userId = data.user ? data.user.id : null;
    const token = data.token || userId;
    delete data.token;
    const res  = {
      status: 200,
      statusText: 'OK',
      headers: new HttpHeaders().append(authEnum.AUTH_HEADER, this._TOKEN_BASE + token),
      body: data
    };
    return of(new HttpResponse(res));
  }

  private _unauthorized(): Observable<HttpErrorResponse> {
    return throwError(new HttpErrorResponse({
      status: 401,
      statusText: 'ERROR',
      error: { errorMessage: 'unauthorized' }
    }));
  }
}

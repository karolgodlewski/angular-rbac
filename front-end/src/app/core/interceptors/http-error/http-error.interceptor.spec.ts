import { TestBed, async } from '@angular/core/testing';
import { AuthService } from '../../services/auth/auth.service';
import { UserService } from '../../services/user/user.service';
import { BehaviorSubject, Subscription, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { apiEndpointsEnum } from '../../../enums/api-endpoints.enum';
import { usersMock } from '../../../mocks/users.mock';
import { HttpErrorInterceptor } from './http-error.interceptor';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

class FakeRouter {
  navigate(d) { }
}

const FakeAuthService = {
  logout() { },
  setAuthorizedStatus(v) { }
};

const _user = new BehaviorSubject({ data: usersMock[1] });
const FakeUserService = {
  user$: _user.asObservable(),
  setUser() { }
};

describe('HttpErrorInterceptor', () => {
  let interceptor: HttpErrorInterceptor;
  let sub: Subscription;
  let authService;
  let router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        HttpErrorInterceptor,
        { provide: AuthService, useValue: FakeAuthService },
        { provide: Router, useClass: FakeRouter }
      ]
    });
    interceptor = TestBed.get(HttpErrorInterceptor);
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
  }));

  afterEach(() => {
    if (sub) {
      sub.unsubscribe();
    }
  });

  describe('intercept', () => {
    it('should intercept 401 res code and logouts the user', (done) => {
      const req = new HttpRequest('GET', apiEndpointsEnum.RANDOM_ENDPOINT);
      const resError = new HttpErrorResponse({ error: {}, status: 401 });
      const next = {
        handle() {
          return throwError(resError);
        }
      };
      const logoutSpy = jest.spyOn(authService, 'logout').mockReturnValue(of(true));
      sub = interceptor.intercept(req, next)
        .pipe(
          catchError((err) => {
            expect(logoutSpy).toHaveBeenCalledTimes(1);
            expect(err).toEqual(resError);
            done();
            return of(err);
          })
        )
        .subscribe();
    });

    it('should intercept 403 res code and navigate user to obtained route', (done) => {
      const req = new HttpRequest('GET', apiEndpointsEnum.RANDOM_ENDPOINT);
      const resError = new HttpErrorResponse({
        error: { statusText: 'MAINTENANCE', redirectTo: '/access-denied' }, status: 403
      });
      const navigateSpy = jest.spyOn(router, 'navigate');
      const next = {
        handle() {
          return throwError(resError);
        }
      };
      sub = interceptor.intercept(req, next)
        .pipe(
          catchError((err) => {
            expect(navigateSpy).toHaveBeenCalledTimes(1);
            expect(err).toEqual(resError);
            done();
            return of(err);
          })
        )
        .subscribe();
    });

    it('sould intercept req and rethrows error', (done) => {
      const req = new HttpRequest('GET', apiEndpointsEnum.RANDOM_ENDPOINT);
      const resError = new HttpErrorResponse({ error: {}, status: 404 });
      const next = {
        handle() {
          return throwError(resError);
        }
      };
      const logoutSpy = jest.spyOn(authService, 'logout').mockReturnValue(of(true));
      sub = interceptor.intercept(req, next)
        .pipe(
          catchError((err) => {
            expect(logoutSpy).not.toHaveBeenCalled();
            expect(err).toEqual(resError);
            done();
            return of(err);
          })
        )
        .subscribe();
    });
  });
});

import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class HttpErrorInterceptor {
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(req)
      .pipe(
        catchError((err) => {
          switch (err.status) {
            case 403:
              if (err.error.statusText === 'MAINTENANCE') {
                this.authService.setAuthorizedStatus(false);
              }
              this.router.navigate([err.error.redirectTo]);
              break;
            case 401:
              if (!this.authService.redirectedToLogin) {
                this.authService.logout().subscribe();
              }
              break;
          }
          return throwError(err);
        })
      );
  }
}

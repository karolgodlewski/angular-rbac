import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpResponse, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { authEnum } from '../../../enums/auth.enum';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthTokenInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(req)
      .pipe(
        tap((res: HttpEvent<any>) => {
          if (res instanceof HttpResponse) {
            const authHeader = res.headers.get(authEnum.AUTH_HEADER);
            if (authHeader) {
              const headerChunks = authHeader.split(' ');
              const token = headerChunks[1];
              localStorage.setItem(authEnum.AUTH_TOKEN_LOCAL_STORAGE_KEY, token);
            } else {
              localStorage.removeItem(authEnum.AUTH_TOKEN_LOCAL_STORAGE_KEY);
            }
          }
          return res;
        }
      ));
  }
}

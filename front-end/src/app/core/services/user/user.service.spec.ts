import { async, TestBed } from '@angular/core/testing';
import { UserService } from './user.service';
import { Subscription } from 'rxjs';
import { skip } from 'rxjs/operators';
import { usersMock } from 'src/app/mocks/users.mock';

describe('UserService', () => {
  let service: UserService;
  let sub: Subscription;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        UserService
      ]
    });
    service = TestBed.get(UserService);
  }));

  afterEach(() => {
    if (sub) {
      sub.unsubscribe();
    }
  });

  describe('user$', () => {
    it('should emit initial user data on subscribtion', (done) => {
      sub = service.user$
        .subscribe((data) => {
          expect(data).toEqual({ data: null });
          done();
        });
    });
  });

  describe('setUser', () => {
    it('should emit the latest available user value', (done) => {
      sub = service.user$
        .pipe(skip(1))
        .subscribe((data) => {
          expect(data).toEqual({ data: usersMock[1] });
          done();
        });
      service.setUser(usersMock[1]);
    });

    it('should clear the user data when called with null', (done) => {
      sub = service.user$
        .pipe(skip(1))
        .subscribe((data) => {
          expect(data).toEqual({ data: null });
          done();
        });
      service.setUser(null);
    });
  });
});

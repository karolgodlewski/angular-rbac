import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import User from 'src/app/interfaces/user/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _userData: User = null;
  private _user: BehaviorSubject<{ data: User }> = new BehaviorSubject({ data: this._userData });
  public readonly user$: Observable<{ data: User }> = this._user.asObservable();

  setUser(userData) {
    this._userData = userData
      ? {
        ...this._userData,
        ...userData
      }
      : null;
    this._user.next({ data: this._userData });
  }
}

import { async, TestBed } from '@angular/core/testing';
import { Subscription, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { HttpService } from '../http/http.service';
import { Router } from '@angular/router';
import { authEnum } from 'src/app/enums/auth.enum';
import { appRoutesEmum } from 'src/app/enums/app-routes.enum';
import { featuresMock } from '../../../mocks/features.mock';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { usersMock } from 'src/app/mocks/users.mock';

const FakeUserService = {
  setUser() { }
};

const FakeHttpService = {
  get() { },
  post() { }
};

const FakeRouter = {
  navigate() { }
};

describe('AuthService', () => {
  let service: AuthService;
  let sub: Subscription;
  let httpService: HttpService;
  let userService: UserService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: UserService, useValue: FakeUserService },
        { provide: Router, useValue: FakeRouter },
        { provide: HttpService, useValue: FakeHttpService }
      ]
    });
    service = TestBed.get(AuthService);
    router = TestBed.get(Router);
    httpService = TestBed.get(HttpService);
    userService = TestBed.get(UserService);
  }));

  afterEach(() => {
    if (sub) {
      sub.unsubscribe();
    }
  });

  describe('login', () => {
    it('should receive correct response', (done) => {
      const ret = {
        status: true,
        user: of(usersMock[1])
      };
      spyOn(httpService, 'post').and.returnValue(of(ret));
      sub = service.login({ username: usersMock[1].username, password: usersMock[1].password })
        .subscribe((res) => {
          expect(res).toEqual(ret);
          done();
        });
    });
  });

  describe('logout', () => {
    it('should receive correct response', (done) => {
      const resBody = { session: false };
      spyOn(httpService, 'post').and.returnValue(of(resBody));
      const removeItemSpy = spyOn(Storage.prototype, 'removeItem');
      const navigateSpy = spyOn(router, 'navigate');
      const setUserSpy = spyOn(userService, 'setUser');
      const setAuthorizedStatusSpy = spyOn(service, 'setAuthorizedStatus');
      service.logout()
        .subscribe((res) => {
          expect(removeItemSpy).toHaveBeenCalledWith(authEnum.AUTH_TOKEN_LOCAL_STORAGE_KEY);
          expect(setUserSpy).toHaveBeenCalledWith(null);
          expect(navigateSpy).toHaveBeenCalledWith([appRoutesEmum.LOGIN]);
          expect(setAuthorizedStatusSpy).toHaveBeenCalledWith(false);
          expect(res).toEqual(resBody);
          done();
        });
    });
  });

  describe('getFeatureData', () => {
    it('should retrieve auth data for provided feature name', (done) => {
      const featureName = 'HOME';
      const ret = { featureData: featuresMock[featureName] };
      spyOn(httpService, 'get').and.returnValue(of(ret));
      sub = service.getFeatureData(featureName)
        .subscribe((res) => {
          expect(res).toEqual(ret);
          done();
        });
      });
    });

  describe('getSessionValidity', () => {
    it('should retrieve session status', (done) => {
      const ret = { session: true };
      spyOn(httpService, 'get').and.returnValue(of(ret));
      sub = service.getSessionValidity()
    .subscribe((res) => {
        expect(res).toEqual(ret);
        done();
      });
    });
  });

  describe('trig401Response', () => {
    it('should result in 401 unauthorized response', (done) => {
      const errRes = { error: 'unauthorized', status: 401 };
      const ret = new HttpErrorResponse(errRes);
      spyOn(httpService, 'get').and.returnValue(throwError(ret));
      service.trig401Response()
        .pipe(
          catchError((err) => {
            expect(err).toEqual(ret);
            done();
            return of(err);
          })
        )
        .subscribe();
    });
  });

  describe('getRedirectToPath', () => {
    it('should retrieve redirect target', () => {
      const ret = service.getRedirectToPath();
      expect(ret).toEqual(service.redirectTo[service.redirectTo.length - 1]);
    });
  });

  describe('setRedirectToPath', () => {
    it('should set redirect target path', () => {
      service.setRedirectToPath(appRoutesEmum.ADMIN_PANEL);
      expect(service.redirectTo[1]).toBe(appRoutesEmum.ADMIN_PANEL);
    });
  });

  describe('setAuthorizedStatus', () => {
    it('should set authorized status', (done) => {
      const status = true;
      service.setAuthorizedStatus(status);
      sub = service.authorized$
        .subscribe((s) => {
          expect(s).toBe(status);
          done();
        });
    });
  });
});

import { Injectable } from '@angular/core';
import { UserService } from '../user/user.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpService } from '../http/http.service';
import { tap } from 'rxjs/operators';
import { authEnum } from 'src/app/enums/auth.enum';
import { Router } from '@angular/router';
import { appRoutesEmum } from 'src/app/enums/app-routes.enum';
import { apiEndpointsEnum } from 'src/app/enums/api-endpoints.enum';
import User from 'src/app/interfaces/user/user.interface';

interface IUserCredentials {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  redirectTo = ['/'];
  redirectBack = false;
  redirectedToLogin = false;

  private _authorized: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public readonly authorized$: Observable<boolean> = this._authorized.asObservable();

  constructor(
    private httpService: HttpService,
    private userService: UserService,
    private router: Router
  ) { }

  login(credentials: IUserCredentials): Observable<{ user: User, token: string }> {
    return this.httpService
      .post<{ user: User, token: string }>(
        apiEndpointsEnum.LOGIN, credentials, { auth: false }
      )
      .pipe(
        tap((res) => {
          this.userService.setUser(res.user);
          this.redirectedToLogin = false;
        })
      );
  }

  logout() {
    return this.httpService.post(apiEndpointsEnum.LOGOUT, null)
      .pipe(
        tap(() => {
          localStorage.removeItem(authEnum.AUTH_TOKEN_LOCAL_STORAGE_KEY);
          this.userService.setUser(null);
          this.setRedirectToPath('/');
          this.setAuthorizedStatus(false);
          this.redirectedToLogin = true;
          this.router.navigate([appRoutesEmum.LOGIN]);
        })
      );
  }

  getFeatureData(featureName: string): Observable<any> {
    return this.httpService.get(apiEndpointsEnum.AUTH_DATA, { queryParams: { name: featureName } });
  }

  getSessionValidity(): Observable<any> {
    return this.httpService.get(apiEndpointsEnum.LOGIN, { auth: true });
  }

  trig401Response(): Observable<any> {
    return this.httpService.get(apiEndpointsEnum.LOGIN, { auth: false });
  }

  getRedirectToPath(): string {
    return this.redirectTo[this.redirectTo.length - 1];
  }

  setRedirectToPath(path: string): void {
    this.redirectTo = [
      this.getRedirectToPath(),
      path
    ];
  }

  setAuthorizedStatus(status: boolean): void {
    this._authorized.next(status);
  }
}

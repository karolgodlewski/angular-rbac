import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpService } from './http.service';
import { Subscription, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { authEnum } from 'src/app/enums/auth.enum';
import { catchError } from 'rxjs/operators';
import { usersMock } from 'src/app/mocks/users.mock';
import { apiEndpointsEnum } from 'src/app/enums/api-endpoints.enum';
import { environment } from 'src/environments/environment';

describe('HttpService', () => {
  const URL = environment.API_URL + '/' + apiEndpointsEnum.RANDOM_ENDPOINT;
  const RES_BODY = { data: 'default response value' };
  const REQ_PAYLOAD = { data: 'some data to be sent' };
  let service: HttpService;
  let sub: Subscription;
  let httpClient: HttpClient;
  let httpTestingCtrl: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.get(HttpService);
    httpClient = TestBed.get(HttpClient);
    httpTestingCtrl = TestBed.get(HttpTestingController);
  }));

  afterEach(() => {
    httpTestingCtrl.verify();
  });

  describe('get', () => {
    it('should call with proper configuration', () => {
      const queryParams = { param1: 'value1', param2: 'value2' };
      const authToken = usersMock[1]._id;
      localStorage.setItem('authToken', authToken);
      sub = service.get(apiEndpointsEnum.RANDOM_ENDPOINT, { queryParams })
        .subscribe(res => expect(res).toEqual(RES_BODY));
      const req = httpTestingCtrl
        .expectOne((r) => {
          const validUrl = r.url === URL + '?param1=value1&param2=value2';
          const validAuthHeader = r.headers.get(authEnum.AUTH_HEADER) === `Bearer ${authToken}`;
          return validUrl && validAuthHeader;
        });
      expect(req.request.method).toEqual('GET');
      const resHeaders = new HttpHeaders()
        .append(authEnum.AUTH_HEADER, `Bearer ${authToken}`);
      req.flush(RES_BODY, { headers: resHeaders });
    });

    it('should call without Authorization header when invoked with auth flag set to false', () => {
      const resBody = { data: 'default response value' };
      localStorage.setItem('authToken', usersMock[1]._id);
      sub = service.get(apiEndpointsEnum.RANDOM_ENDPOINT, { auth: false })
        .subscribe((res) => {
          expect(res).toEqual(resBody);
        });
      const req = httpTestingCtrl
        .expectOne((r) => {
          const validUrl = r.url === URL;
          const validAuthHeader = r.headers.get(authEnum.AUTH_HEADER) === null;
          return validUrl && validAuthHeader;
        });
      expect(req.request.method).toEqual('GET');
      req.flush(resBody);
    });

    it('should catch and rethrow error when received error response', () => {
      const resError = new ErrorEvent('Http error', { message: 'unauthorized' });
      sub = service.get(apiEndpointsEnum.RANDOM_ENDPOINT)
        .pipe(
          catchError((err) => {
            expect(err.error.message).toEqual('unauthorized');
            return of(true);
          })
        )
        .subscribe();
      const req = httpTestingCtrl.expectOne(URL);
      expect(req.request.method).toEqual('GET');
      req.error(resError, { status: 401, statusText: 'error' });
    });

    it('should skip query param when its value not defined', () => {
      const queryParams = { param1: null, param2: 'value2' };
      sub = service.get(apiEndpointsEnum.RANDOM_ENDPOINT, { queryParams })
        .subscribe();
      const req = httpTestingCtrl.expectOne(URL + '?param2=value2');
      expect(req.request.method).toEqual('GET');
      req.flush(RES_BODY);
    });

    it('should not set Authorization header when invalid value provided', () => {
      sub = service.get(apiEndpointsEnum.RANDOM_ENDPOINT).subscribe();
      const req = httpTestingCtrl.expectOne(URL);
      expect(req.request.method).toEqual('GET');
      const resHeaders = new HttpHeaders()
        .append(authEnum.AUTH_HEADER, `Bearer1234`);
      req.flush(RES_BODY, { headers: resHeaders });
    });
  });

  describe('post', () => {
    it('should call with desired configuration and payload when post invoked', () => {
      const resBody = { data: 'default response value' };
      const authToken = usersMock[1]._id;
      sub = service.post(apiEndpointsEnum.RANDOM_ENDPOINT, REQ_PAYLOAD)
        .subscribe((res) => expect(res).toEqual(resBody));
      const req = httpTestingCtrl
        .expectOne((r) => {
          const validUrl = r.url === URL;
          const validAuthHeader = r.headers.get(authEnum.AUTH_HEADER) === `Bearer ${usersMock[1]._id}`;
          const validPayload = r.body.data === REQ_PAYLOAD.data;
          return validUrl && validAuthHeader && validPayload;
        });
      expect(req.request.method).toEqual('POST');
      const resHeaders = new HttpHeaders()
        .append(authEnum.AUTH_HEADER, `Bearer ${authToken}`);
      req.flush(resBody, { headers: resHeaders });
    });

    it('should call without Authorization header when invoked with auth flag set to false', () => {
      const resBody = { data: 'default response value' };
      localStorage.setItem('authToken', usersMock[1]._id);
      sub = service.post(apiEndpointsEnum.RANDOM_ENDPOINT, REQ_PAYLOAD, { auth: false })
        .subscribe((res) => {
          expect(res).toEqual(resBody);
        });
      const req = httpTestingCtrl.expectOne((r) => {
        const validUrl = r.url === URL;
        const validAuthHeader = r.headers.get(authEnum.AUTH_HEADER) === null;
        return validUrl && validAuthHeader;
      });
      expect(req.request.method).toEqual('POST');
      req.flush(resBody);
    });

    it('should catch and rethrow error when obtained error response', () => {
      const resError = new ErrorEvent('Http error', { message: 'unauthorized' });
      sub = service.post(apiEndpointsEnum.RANDOM_ENDPOINT, { })
        .pipe(
          catchError((err) => {
            expect(err.error.message).toEqual('unauthorized');
            return of(true);
          })
        )
        .subscribe();
      const req = httpTestingCtrl.expectOne(URL);
      expect(req.request.method).toEqual('POST');
      req.error(resError, { status: 401, statusText: 'error' });
    });
  });
});

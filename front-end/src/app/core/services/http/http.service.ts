import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { IHttpReqOptionalConfig } from 'src/app/interfaces/http/http-req-optional-config.interface';
import { HttpConfig } from 'src/app/interfaces/http/HttpConfig';
import { authEnum } from 'src/app/enums/auth.enum';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private _httpConfig: HttpConfig;

  constructor(private http: HttpClient) { }

  get<T>(endpoint: string, optional?: IHttpReqOptionalConfig)
  : Observable<T> {
    this._httpConfig = new HttpConfig(optional || {});
    return this.http
      .get(this._getRequestUrl(endpoint), this._getReqHeaders())
      .pipe(
        map((res: HttpResponse<any>) => {
          return res.body;
        }),
        catchError(this._errorHandler)
      );
  }

  post<T>(endpoint: string, body: unknown, optional?: IHttpReqOptionalConfig)
  : Observable<T> {
    this._httpConfig = new HttpConfig(optional || {});
    return this.http
      .post(this._getRequestUrl(endpoint), body, this._getReqHeaders())
      .pipe(
        map((res: any) => {
          return res.body;
        }),
        catchError(this._errorHandler)
      );
  }

  private _getRequestUrl(endpoint: string): string {
    let url = `${this._httpConfig.apiUrl}/${endpoint}`;
    const queryString = this._getQueryString();
    if (queryString) {
      url += queryString;
    }
    return url;
  }

  private _getReqHeaders(): { headers: HttpHeaders, observe: any } {
    const headers = {
      'Content-Type': 'application/json'
    } as any;
    const authToken = localStorage.getItem(authEnum.AUTH_TOKEN_LOCAL_STORAGE_KEY);
    if (this._httpConfig.auth && authToken) {
      const authHeader = `Bearer ${authToken}`;
      headers[authEnum.AUTH_HEADER] = authHeader;
    }
    return {
      headers,
      observe: 'response'
    };
  }

  private _getQueryString(): string {
    const { queryParams } = this._httpConfig;
    const qpEntries = Object.entries(queryParams);
    if (!qpEntries.length) {
      return '';
    }
    const qp = [];
    const queryParamsMap = new Map(qpEntries);
    queryParamsMap.forEach((val, key) => {
      if (val) {
        qp.push([key, val]);
      }
    });
    return '?' + qp.map(([key, val]) => `${key}=${val}`).join('&');
  }

  private _errorHandler(error): Observable<never> {
    console.error(`An error ${error.status} occured: ${error.message}`);
    return throwError(error);
  }
}
